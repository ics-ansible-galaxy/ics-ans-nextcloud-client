import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('nextcloud_client')


def test_nextcloud_client_is_installed(host):
    nextcloud_client = host.package("nextcloud-client")
    assert nextcloud_client.is_installed
