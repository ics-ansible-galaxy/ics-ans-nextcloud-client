# ics-ans-nextcloud-client

Ansible playbook to install nextcloud-client.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
